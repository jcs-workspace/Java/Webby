package core;

public class StatusUpdater implements Runnable {
    
    private Thread thread = null;
    
    
    @Override
    public void run() {
        
        while (true) {
            
            if (HttpDownloadUtility.CONTENT_LENGTH != 0) {
                // calculate percentage.
                HttpDownloadUtility.TASK_PERCENTAGE = 
                        (float)HttpDownloadUtility.PROGRESS_LENGTH / 
                        (float)HttpDownloadUtility.CONTENT_LENGTH * 
                        100.0f;
            }
            
            // -- log --
            System.out.println("Percentage: " + HttpDownloadUtility.TASK_PERCENTAGE + " %");
            System.out.println("Progress: " + HttpDownloadUtility.PROGRESS_LENGTH);
            System.out.println("Content: " + HttpDownloadUtility.CONTENT_LENGTH);
            // -- log --
            
            
        
            if (JcsGUI_RawVideoDownloader.RAW_VIDEO_DOWNLOADER_PROGRESS_BAR != null) {
                JcsGUI_RawVideoDownloader.RAW_VIDEO_DOWNLOADER_PROGRESS_BAR.setValue((int)HttpDownloadUtility.TASK_PERCENTAGE);
            }
            
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
    
    public void start() {
        
        if (thread != null)
            return;
        
        thread = new Thread(this);
        thread.start();
    }
    
}
