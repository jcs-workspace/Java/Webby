package core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;

import org.apache.commons.io.FilenameUtils;

/**
* A utility that downloads a file from a URL.
* @author www.codejava.net
*
*/
public class HttpDownloadUtility {
    
    public static long CONTENT_LENGTH = 0;
    public static long PROGRESS_LENGTH = 0;
    public static float TASK_PERCENTAGE = 0;
    
    // 4 KB
    private static final int BUFFER_SIZE = 1024 * 4 * 16;
    
    /**
     * Desc
     * 
     * @param fileURL
     * @param saveDir
     * @param inputFileName
     * @return
     * @throws IOException
     */
    public static boolean downloadFile(String fileURL, String saveDir, final String inputFileName) 
            throws IOException {
        
        URL url = new URL(fileURL);
        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
        httpConn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
        int responseCode = httpConn.getResponseCode();

        // always check HTTP response code first
        if (responseCode == HttpURLConnection.HTTP_OK) {
            String fileName = "";
            String disposition = httpConn.getHeaderField("Content-Disposition");
            String contentType = httpConn.getContentType();
            int contentLength = httpConn.getContentLength();
            CONTENT_LENGTH = contentLength;

            if (disposition != null) {
                // extracts file name from header field
                int index = disposition.indexOf("filename=");
                if (index > 0) {
                    fileName = disposition.substring(index + 10,
                            disposition.length() - 1);
                }
            } else {
                // extracts file name from URL
                fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1,
                        fileURL.length());
            }

            System.out.println("Content-Type = " + contentType);
            System.out.println("Content-Disposition = " + disposition);
            System.out.println("Content-Length = " + contentLength);
            System.out.println("fileName = " + fileName);

            // get the extension of the file.
            String fileExtension = FilenameUtils.getExtension(fileName);
            
            // opens input stream from the HTTP connection
            InputStream inputStream = httpConn.getInputStream();
            String saveFilePath = saveDir + File.separator + inputFileName + "." + fileExtension;

            //System.out.println(saveFilePath);
            
            // opens an output stream to save into file
            FileOutputStream outputStream = new FileOutputStream(saveFilePath);
            FileChannel outc = outputStream.getChannel();
            ReadableByteChannel inChannel = Channels.newChannel(inputStream);

            int bytesRead = -1;
            
            // initialize the content length
            PROGRESS_LENGTH = 0;
            
            // NOTE(jenchieh): old code.
            //byte[] buffer = new byte[BUFFER_SIZE];
            ByteBuffer byteBuffer = ByteBuffer.allocate(BUFFER_SIZE);
            
            while ((bytesRead = inChannel.read(byteBuffer)) != -1) {
                
                // NOTE(jenchieh): old code.
                //outputStream.write(buffer, 0, bytesRead);
                
                byteBuffer.rewind();
                byteBuffer.limit(bytesRead);
                
                // NOTE(jenchieh): still testing.
                //PROGRESS_LENGTH += outc.transferFrom(inChannel, 0, bytesRead);
                
                // add up the progress length. lazy code.
                PROGRESS_LENGTH += bytesRead;
                
                while (bytesRead > 0) {
                    bytesRead -= outc.write(byteBuffer);
                }
                
                byteBuffer.clear();
            }
            
            // verify the result
            if (PROGRESS_LENGTH == CONTENT_LENGTH) {
                System.out.println("Copied " + CONTENT_LENGTH + " bytes successfully!");
            }

            outputStream.close();
            inputStream.close();
            outc.close();
            inChannel.close();

            System.out.println("File downloaded!");
        } else {
            System.out
                    .println("No file to download. Server replied HTTP code: "
                            + responseCode);
            
            // failed to download the video
            return false;
        }
        
        // close the connection.
        httpConn.disconnect();
        
        // success to download the video
        return true;
    }
    
}
