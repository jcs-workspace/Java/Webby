package core;

import java.io.IOException;

import javax.swing.JFrame;

/**
 * 
 * @author JenChieh
 *
 */
public class ProgramMain {

    private static int SCREEN_WIDTH = 600 + 275;
    private static int SCREEN_HEIGHT = 175 + 50;
    
    public static void main(final String[] args) throws IOException {
        
        JcsGUI_RawVideoDownloader f = new JcsGUI_RawVideoDownloader();
        
        // set size out after "new operator" will override the size
        f.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
        f.updateScreenLocation();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
    }
}
