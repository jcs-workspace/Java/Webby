package core;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

public class URLReader {

    
    
    public static void main(final String[] args) throws Exception{
        URL webPage = new URL("http://www.eyny.com/video.php?mod=video&vid=1484039");
        
        BufferedReader in = new BufferedReader(new InputStreamReader(webPage.openStream()));
        
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            System.out.println(inputLine);
        }
        
        // close the stream
        in.close();
    }
}
