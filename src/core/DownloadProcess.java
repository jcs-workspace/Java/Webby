package core;

import java.io.IOException;

import javax.swing.JOptionPane;

public class DownloadProcess implements Runnable {

    private Thread process = null;
    
    private String downloadLink = "";
    private String output_path = "";
    private String file_name = "";
    
    public DownloadProcess(String downloadLink, String output_path, String file_name) {
        this.downloadLink = downloadLink;
        this.output_path = output_path;
        this.file_name = file_name;
    }
    
    /**
     * main loop for thread.
     */
    @Override
    public void run() {
        
        boolean success = false;
        boolean direct_link_was_enabled = false;
        
        try {
            // disable button
            JcsGUI_RawVideoDownloader.DOWNLOAD_BUTTON.setEnabled(false);
            JcsGUI_RawVideoDownloader.SEARCH_BUTTON.setEnabled(false);
            
            if (JcsGUI_RawVideoDownloader.direct_url_textField.isEnabled())
                direct_link_was_enabled = true;
            
            JcsGUI_RawVideoDownloader.direct_url_textField.setEnabled(false);
            JcsGUI_RawVideoDownloader.url_textField.setEnabled(false);
            
            JcsGUI_RawVideoDownloader.DOWNLOADING_LABEL.setText("Downloading...");
            
            // start download!!
            JcsGUI_RawVideoDownloader.DOWNLOADING = true;
            
            // start donwload.
            success = HttpDownloadUtility.downloadFile(downloadLink,
                    output_path, file_name);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        if (!success) {
            // video are not valid to download.
            JOptionPane.showMessageDialog(null,
                    "Failed to download the video for unknown reason...");
            
            JcsGUI_RawVideoDownloader.DOWNLOADING_LABEL.setText("Failed...");
        } else {
            JOptionPane.showMessageDialog(null, "Download Success!");
            JcsGUI_RawVideoDownloader.DOWNLOADING_LABEL.setText("Succeed!!!");
        }
        
        // enable button
        JcsGUI_RawVideoDownloader.DOWNLOAD_BUTTON.setEnabled(true);
        JcsGUI_RawVideoDownloader.SEARCH_BUTTON.setEnabled(true);
        JcsGUI_RawVideoDownloader.DOWNLOADING = false;
        
        if (direct_link_was_enabled)
            JcsGUI_RawVideoDownloader.direct_url_textField.setEnabled(true);
        else
            JcsGUI_RawVideoDownloader.url_textField.setEnabled(true);
    }
    
    /**
     * Start the thread.
     */
    public void start() {
        
        // this thread can only be start once.
        if (process != null)
            return;
        
        process = new Thread(this);
        process.start();
    }
    
}
