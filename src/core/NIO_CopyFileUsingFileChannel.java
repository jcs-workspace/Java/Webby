package core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

/**
 * 
 * Copy file using java.nio.channels.FileChannel
 * SOURE(jenchieh): https://www.youtube.com/watch?v=Yg1-zOhVCdA
 * 
 * @author JenChieh<jcs090218@gmail.com>
 *
 */
public class NIO_CopyFileUsingFileChannel {
    
    
    public static void main(final String[] args) {
        String srcFileName = "C:/Users/JenChieh/Downloads/Raw Video Downloader by Webby.jar";
        String dstFileName = "C:/Users/JenChieh/Downloads/Raw Video Downloader by Webby2.jar";
        
        try {
            copyFile(srcFileName, dstFileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private static void copyFile(final String srcFileName, final String dstFileName) 
            throws IOException {
        
        File srcFile = new File(srcFileName);
        File dstFile = new File(dstFileName);
        
        if (!dstFile.exists()) {
            dstFile.createNewFile();
        }
        
        FileInputStream in = null;
        FileOutputStream out = null;
        FileChannel inc = null;
        FileChannel outc = null;
        
        try {
            in = new FileInputStream(srcFile);
            out = new FileOutputStream(dstFile);
            inc = in.getChannel();
            outc = out.getChannel();
            
            long count = 0;
            long size = inc.size();
            while (count < size) {
                count += outc.transferFrom(inc, 0, size - count);
            }
            
            // verify the result
            if (outc.size() == size) {
                System.out.println("Copied " + size + " bytes successfully!");
            }
            
        } finally {
            in.close();
            out.close();
            inc.close();
            outc.close();
        }
        
    }
    
}
