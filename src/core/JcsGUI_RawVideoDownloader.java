package core;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

/**
 * 
 * GUI controller
 * 
 * @author JenChieh
 * 
 */
public class JcsGUI_RawVideoDownloader extends JFrame {

    // progress bar
    public static JProgressBar RAW_VIDEO_DOWNLOADER_PROGRESS_BAR = null;
    public static JLabel DOWNLOADING_LABEL = new JLabel();

    private static final long serialVersionUID = -9021800001L;

    private static final String DEFUALT_DOWNLOAD_FILENAME = "Webby - Defult File Name";

    private String file_name = "";

    private String output_path = System.getProperty("user.home");
    private JLabel output_label = new JLabel("Output Directory:    "
            + System.getProperty("user.home"), JLabel.RIGHT);
    private JLabel downloadLink_label = new JLabel("Download Link: none ");

    private JTextField fileName_textField = new JTextField(20);
    public static JTextField url_textField = new JTextField(20);
    public static JTextField direct_url_textField = new JTextField(20);

    private JList<String> allPossibleDownloadURLList = new JList<String>();
    private DefaultListModel<String> defaultListModel = new DefaultListModel<String>();
    private String downloadLink = "";
    
    /* Buttons */
    public static JButton SEARCH_BUTTON = new JButton("Search");
    public static JButton DOWNLOAD_BUTTON = new JButton("Download");
    
    public static boolean DOWNLOADING = false;
    

    public JcsGUI_RawVideoDownloader() {
        super("Raw Video Downloader by Webby");

        // -----------------------------------------------------
        JPanel file_panel = new JPanel();
        JPanel result_panel = new JPanel();
        JPanel west_panel = new JPanel();

        file_panel.setLayout(new FlowLayout(FlowLayout.LEFT));

        getContentPane().add(file_panel, BorderLayout.NORTH);
        getContentPane().add(result_panel, BorderLayout.SOUTH);
        getContentPane().add(west_panel, BorderLayout.WEST);

        JPanel center_panel = new JPanel();
        JPanel center_right_panel = new JPanel();
        JPanel center_left_panel = new JPanel();

        center_left_panel.setLayout(new FlowLayout(FlowLayout.LEFT));
        center_right_panel.setLayout(new FlowLayout(FlowLayout.RIGHT));

        center_panel.add(center_right_panel);
        center_panel.add(center_left_panel);
        getContentPane().add(center_panel, BorderLayout.EAST);

        // NORTH
        {
            JLabel url_label = new JLabel("Video URL: ");
            file_panel.add(url_label);

            url_textField.setToolTipText("Video Url");
            url_textField.addMouseListener(
                    new MouseAdapter() {
                        @Override
                        public void mouseClicked(MouseEvent ev) {
                            
                            // TODO(jenchieh): this is the bad design cuz multi-thread 
                            // will have the problem.
                            if (DOWNLOADING)
                                return;
                            
                            direct_url_textField.setEnabled(false);
                            url_textField.setEnabled(true);
                            
                            // set the cursor in JTextField!
                            url_textField.requestFocusInWindow();
                        }
                    });
            file_panel.add(url_textField);
            
            JLabel direct_url_label = new JLabel("Direct Link: ");
            west_panel.add(direct_url_label);

            direct_url_textField.setToolTipText("Direct Url");
            
            direct_url_textField.addMouseListener(
                    new MouseAdapter() {
                        @Override
                        public void mouseClicked(MouseEvent ev) {

                            // TODO(jenchieh): this is the bad design cuz multi-thread 
                            // will have the problem.
                            if (DOWNLOADING)
                                return;
                            
                            url_textField.setEnabled(false);
                            direct_url_textField.setEnabled(true);
                            
                            // set the cursor in JTextField!
                            direct_url_textField.requestFocusInWindow();
                        }
                    });
            // set default as not enable.
            direct_url_textField.setEnabled(false);
            west_panel.add(direct_url_textField);

            SEARCH_BUTTON = new JButton("Search");
            SEARCH_BUTTON.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent ev) {
                        searchButtonClick();
                    }
                });
            file_panel.add(SEARCH_BUTTON);

            JLabel fileName_label = new JLabel("File Name: ");
            file_panel.add(fileName_label);

            // Text Field
            fileName_textField.setToolTipText("Video File Name");
            fileName_textField.setText(DEFUALT_DOWNLOAD_FILENAME);
            file_panel.add(fileName_textField);

            // Button
            JButton output_dir = new JButton("Select Output Directory");
            output_dir.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                    chooseDirectory();
                }
            });
            file_panel.add(output_dir);

        }

        // CENTER
        {
            allPossibleDownloadURLList
                    .setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            allPossibleDownloadURLList.setLayoutOrientation(JList.VERTICAL);
            allPossibleDownloadURLList.setVisibleRowCount(10);
            
            // update the gui
            new StatusUpdater().start();

            allPossibleDownloadURLList.setModel(defaultListModel);
            defaultListModel.addElement("none");

            JScrollPane listScroller = new JScrollPane(
                    allPossibleDownloadURLList);
            listScroller.setPreferredSize(new Dimension(250, 80));

            center_left_panel.add(listScroller);
            
            center_right_panel.add(output_label); // Label
        }

        // SOUTH
        {
            RAW_VIDEO_DOWNLOADER_PROGRESS_BAR = new JProgressBar(0, 100);
            RAW_VIDEO_DOWNLOADER_PROGRESS_BAR.setValue(0);
            RAW_VIDEO_DOWNLOADER_PROGRESS_BAR.setStringPainted(true);
            result_panel.add(RAW_VIDEO_DOWNLOADER_PROGRESS_BAR);

            DOWNLOAD_BUTTON = new JButton("Download Video");
            DOWNLOAD_BUTTON.addActionListener(

            new ActionListener() {

                public void actionPerformed(ActionEvent ev) {
                    try {
                        downloadButtonClick();
                        startProgressBar();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });

            result_panel.add(DOWNLOAD_BUTTON);
            result_panel.add(DOWNLOADING_LABEL);
        }

        // -----------------------------------------------------

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
        setResizable(false);
        updateScreenLocation();
    }

    /**
     * Update screen location to center.
     */
    public void updateScreenLocation() {
        Toolkit tk = Toolkit.getDefaultToolkit();
        Dimension dim = tk.getScreenSize();
        int xPos = (dim.width / 2) - (this.getWidth() / 2);
        int yPos = (dim.height / 2) - (this.getHeight() / 2);

        this.setLocation(xPos, yPos);
    }

    /**
     * 
     */
    public void searchButtonClick() {
        
        // get the download link
        getAddressFromPageSource(url_textField.getText());

        if (downloadLink.equals("")) {
            JOptionPane
                    .showMessageDialog(null,
                            "Please enter the download URL or the link you enter is invalid...");
        }
    }

    /**
     * Do stuff when the download button is click.
     * 
     * @throws IOException
     */
    public void downloadButtonClick() throws IOException {

        if (url_textField.isEnabled()) {
            // TODO(jenchieh): nothing here...
        }
        else if (direct_url_textField.isEnabled()) {
            // update download link is using direct link!
            downloadLink = direct_url_textField.getText();
        }
        
        if (downloadLink.equals("")) {
            JOptionPane
                    .showMessageDialog(null,
                            "Please enter the download URL or the link you enter is invalid...");
            return;
        }
    
        file_name = fileName_textField.getText();
        if (file_name.equals("")) {
            file_name = DEFUALT_DOWNLOAD_FILENAME;
        }
    
        if (output_path.equals("")) {
            JOptionPane.showMessageDialog(null,
                    "Please Select the output directory...");
            return;
        }
        
        DownloadProcess dp = new DownloadProcess(downloadLink, output_path, file_name);
        dp.start();
    }

    /**
     * 
     * @return
     * @throws IOException
     */
    public void getAddressFromPageSource(final String url) {

        String inputLine = "";

        try {
            URL webPage = new URL(url);
            URLConnection yc = webPage.openConnection();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    yc.getInputStream(), "UTF-8"));

            // clear the jlist, so make it look like it got refresh.
            defaultListModel.clear();

            // read the page source.
            while ((inputLine = in.readLine()) != null) {

                System.out.println(inputLine);

                if (inputLine.contains("file: ")) {
                    // add the url link to jlist
                    defaultListModel.addElement(getURLInline(inputLine));
                }

                inputLine = "";
            }

            // check empty
            if (defaultListModel.isEmpty()) {
                defaultListModel.addElement("none");
                JOptionPane.showMessageDialog(null,
                        "No video found in source page...");
            } else {
                downloadLink = defaultListModel.get(0);
                downloadLink_label.setText("Download Link: " + downloadLink);
                System.out.println(downloadLink);
            }

            // close the stream
            in.close();

        } catch (MalformedURLException e) {
            System.err.print("MalformedURLException: " + e);
        } catch (IOException e) {
            System.err.print("IOException: " + e);
        }
    }

    /**
     * Open the file dialogue
     */
    public void chooseDirectory() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new File(System
                .getProperty("user.home")));
        fileChooser.setDialogTitle("Select the output directory...");
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        //
        // disable the "All files" option.
        //
        fileChooser.setAcceptAllFileFilterUsed(false);

        //
        if (fileChooser.showOpenDialog(this) != JFileChooser.APPROVE_OPTION) {
            System.out.println("No Selection ");
            return;
        }

        System.out.println("getCurrentDirectory(): "
                + fileChooser.getCurrentDirectory());
        System.out.println("getSelectedFile() : "
                + fileChooser.getSelectedFile());

        output_path = fileChooser.getSelectedFile().toString();
        output_path = output_path.replace("\\", "/");
        output_path += "/";
        System.out.println("\nDirectory Path: " + output_path + "\n");

        updateLabel();
    }

    /**
     * Update the java swing label
     */
    public void updateLabel() {

        if (output_path.equals(""))
            return;

        output_label.setText("Output Directory: " + output_path);
    }

    /**
     * Originally have file: http//..... delete the first few character and the
     * last character.
     * 
     * @return
     */
    private String getURLInline(String url) {
        return url.substring(7, url.length() - 1);
    }

    /**
     * Reset the progress bar.
     */
    public static void resetProgressBar() {
        RAW_VIDEO_DOWNLOADER_PROGRESS_BAR.setValue(0);
        RAW_VIDEO_DOWNLOADER_PROGRESS_BAR.setStringPainted(true);
    }

    /**
     * Reset the progress bar.
     */
    public static void setProgressBarValue(final int val) {
        RAW_VIDEO_DOWNLOADER_PROGRESS_BAR.setValue(val);
        
        DOWNLOADING_LABEL.setText("Downloading...");
        
    }
    
    /**
     * 
     */
    private void startProgressBar() {
        
    }
    
}
